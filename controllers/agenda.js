const AgendaModel = require("../models/agenda");

exports.getAll = async function(req, res) {
    try {
        const result = await AgendaModel.getAll(req.session.authenticated.id);

        const retorno = [];

        for (let i = 0; i < result.rowCount; i++) {
            const current = result.rows[i];
   
            retorno.push({
                id: current.id,
                startDate: new Date(current.data_inicio).toISOString().substring(0, 16),
                endDate: new Date(current.data_fim).toISOString().substring(0, 16),
                title: current.titulo,
                notes: current.informacoes 
            });
        } 

        return res.send(retorno);
    } catch(err) {
        console.log(err);
        return res.sendStatus(500);
    }
}

exports.insert = async function(req, res) {
    try {
        const info = req.body;
        const data_inicio = info.data_inicio;
        const data_fim = info.data_fim;
        const titulo = info.titulo;
        const informacoes = info.informacoes;
        const usuario_criou = req.session.authenticated.id;

        await AgendaModel.insert(usuario_criou, data_inicio, data_fim, titulo, informacoes);

        return res.sendStatus(201);
    } catch(err) {
        console.log(err)
        return res.sendStatus(500);
    }
}

exports.update = async function(req, res) {
    try {
        const usuario_criou = req.session.authenticated.id;

        AgendaModel.update(req.params.id, req.body, usuario_criou);

        return res.sendStatus(200);
    } catch(err) {
        console.log(err);
        return res.sendStatus(500);
    }
}