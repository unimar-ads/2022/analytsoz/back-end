const VendaModel = require("../models/venda");
const ItemvendaController = require("../controllers/itemvenda");

exports.getAll = async function(req, res, next) {
    
}

exports.get = async function(req, res, next) {

}

exports.insert = async function(req, res, next) {
    try {
        const dados = req.body;
        const cliente_venda = dados.cliente_venda;
        const itens = dados.itens;

        const venda = await VendaModel.insert(req.session.authenticated.id, cliente_venda);

        if (venda.rows.length == 0) return res.sendStatus(403);

        const result = await ItemvendaController.insert(venda.rows[0].id, itens);

        return res.sendStatus(201);
    } catch(error) {
        console.log(error);
        return res.sendStatus(500);
    }
}

exports.historicoVendas = async function(req, res, next) {
    try {
        const vendas = await VendaModel.historicoVendas(req.session.authenticated.id);

        for (let i = 0; i < vendas.rowCount; i++) {
            vendas.rows[i].data_criacao = new Date(vendas.rows[i].data_criacao).toLocaleDateString("pt-BR");
        }

        return res.send(vendas.rows);
    } catch(error) {
        console.log(error);
        return res.sendStatus(500);
    }
}

exports.detalhesVenda = async function(req, res, next) {
    try {
        const id_logado = req.session.authenticated.id
        const id_venda = req.params.id;

        const detalhes = await VendaModel.detalhesVenda(id_venda, id_logado);
        const produtos = await VendaModel.produtosVenda(id_venda, id_logado);
        const servicos = await VendaModel.servicosVenda(id_venda, id_logado);

        return res.send({
            detalhes: detalhes.rows[0],
            produtos: produtos.rows,
            servicos: servicos.rows
        });
    } catch(error) {
        console.log(error);
        return res.sendStatus(500);
    }
}

exports.update = async function(req, res, next) {
    
}