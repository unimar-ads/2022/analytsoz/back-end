const FaturamentoModel = require("../models/faturamento");

exports.vendasProdutoServico = async function(req, res) {
    try {
        const usuario_logado = req.session.authenticated.id;

        const result = await FaturamentoModel.vendasProdutoServico(usuario_logado, req.params.dias);
        const servicosVendidos = await FaturamentoModel.servicosMaisvendidos(usuario_logado, req.params.dias);
        const produtosVendidos = await FaturamentoModel.produtosMaisvendidos(usuario_logado, req.params.dias);

        const rowsServicos = servicosVendidos.rows;
        const rowsProdutos = produtosVendidos.rows;
        const rows = result.rows;

        const retorno = {
            vendas: {
                keys: [],
                values: []
            },
            valorganho: {
                keys: [],
                values: []
            },
            maisvendidos: {
                produtos: {
                    keys: [],
                    values: []
                },
                servicos: {
                    keys: [],
                    values: []
                }
            }
        };

        for (let i = 0; i < result.rowCount; i++) {
            retorno.vendas.keys.push(rows[i].categoria);
            retorno.vendas.values.push(rows[i].total_vendas);

            retorno.valorganho.keys.push(rows[i].categoria);
            retorno.valorganho.values.push(rows[i].valor_total);
        } 

        for (let i = 0; i < servicosVendidos.rowCount; i++) {
            retorno.maisvendidos.servicos.keys.push(rowsServicos[i].descricao);
            retorno.maisvendidos.servicos.values.push(rowsServicos[i].quantidade);
        } 

        for (let i = 0; i < produtosVendidos.rowCount; i++) {
            retorno.maisvendidos.produtos.keys.push(rowsProdutos[i].nome);
            retorno.maisvendidos.produtos.values.push(rowsProdutos[i].quantidade);
        } 

        return res.send(retorno);
    } catch(err) {
        console.log(err);
        return res.sendStatus(500);
    }
}