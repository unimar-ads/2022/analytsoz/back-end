const validator = require("express-joi-validation").createValidator({});
const custoValidator = require("../validators/custo");

const router = require("express").Router();
const CustoController = require("../controllers/custo");

router.get("/", CustoController.getAll);
router.get("/:id", CustoController.get);
router.post("/", CustoController.insert);
router.put("/:id", validator.body(custoValidator.allowedUpdate), CustoController.update);

module.exports = router;