const router = require("express").Router();
const VendaController = require("../controllers/venda");

router.post("/", VendaController.insert);
router.get("/historicoVendas", VendaController.historicoVendas);
router.get("/detalhesVenda/:id", VendaController.detalhesVenda);

module.exports = router;