const validator = require("express-joi-validation").createValidator({});
const servicoValidator = require("../validators/servico");

const router = require("express").Router();
const ServicoController = require("../controllers/servico");

router.get("/", ServicoController.getAll);
router.get("/:id", ServicoController.get);
router.post("/", validator.body(servicoValidator.columns), ServicoController.insert);
router.put("/:id", validator.body(servicoValidator.allowedUpdate), ServicoController.update);

module.exports = router;