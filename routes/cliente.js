const validator = require("express-joi-validation").createValidator({});
const clienteValidator = require("../validators/cliente");

const router = require("express").Router();
const ClienteController = require("../controllers/cliente");

router.get("/", ClienteController.getAll);
router.get("/:id", ClienteController.get);
router.post("/", validator.body(clienteValidator.columns), ClienteController.insert);
router.put("/:id", validator.body(clienteValidator.allowedUpdate), ClienteController.update);

module.exports = router;