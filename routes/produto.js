const validator = require("express-joi-validation").createValidator({});
const produtoValidator = require("../validators/produto");

const router = require("express").Router();
const ProdutoController = require("../controllers/produto");

router.get("/", ProdutoController.getAll);
router.get("/:id", ProdutoController.get);
router.post("/", validator.body(produtoValidator.columns), ProdutoController.insert);
router.put("/:id", validator.body(produtoValidator.allowedUpdate), ProdutoController.update);

module.exports = router;