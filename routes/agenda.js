const router = require("express").Router();
const AgendaController = require("../controllers/agenda");

router.get("/", AgendaController.getAll);
router.post("/", AgendaController.insert);
router.put("/:id", AgendaController.update);

module.exports = router;