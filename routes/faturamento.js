const router = require("express").Router();
const FaturamentoController = require("../controllers/faturamento");

router.get("/vendasProdutoServico/:dias", FaturamentoController.vendasProdutoServico);

module.exports = router;