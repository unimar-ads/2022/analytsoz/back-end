require("dotenv").config();

const express           = require("express");
const app               = express();
const sessions          = require("express-session");
const https             = require("https");
const fs                = require("fs");

const ProdutoRoutes     = require("./routes/produto");
const ServicoRoutes     = require("./routes/servico");
const UsuarioRoutes     = require("./routes/usuario");
const VendaRoutes       = require("./routes/venda");
const ClienteRoutes     = require("./routes/cliente");
const CustoRoutes       = require("./routes/custo");
const FaturamentoRoutes = require("./routes/faturamento");
const AgendaRoutes      = require("./routes/agenda");

const checkAuth         = require("./middlewares/checkAuth");

app.use(sessions({
    secret: process.env.SESSION_SECRET,
    resave: true,
    saveUninitialized: false,
    cookie: { 
        maxAge: 600000 * 10,
        sameSite: process.env.NODE_ENV === "production" ? "none" : "lax",
        secure: process.env.NODE_ENV === "production",
        httpOnly: true
    }
}));

app.use(
    require("cors")({origin: ["http://localhost:3000", "http://www.analytcoz.site", "http://analytcoz.site"], credentials: true}),
    express.json(),
    express.urlencoded({extended: true})
);

app.use("/produto", checkAuth, ProdutoRoutes);
app.use("/servico", checkAuth, ServicoRoutes);
app.use("/venda", checkAuth, VendaRoutes);
app.use("/cliente", checkAuth, ClienteRoutes);
app.use("/custo", checkAuth, CustoRoutes);
app.use("/faturamento", checkAuth, FaturamentoRoutes);
app.use("/agenda", checkAuth, AgendaRoutes);
app.use("/usuario", UsuarioRoutes);

app.listen(process.env.PORT || 8000,  () => console.log("Servidor inciado"));