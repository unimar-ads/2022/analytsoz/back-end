const Joi = require("joi");

exports.columns = Joi.object({
    descricao: Joi.string().required(),
    valor: Joi.number().required(),
    tempo_gasto: Joi.number().required(),
});

exports.allowedUpdate = Joi.object({
    descricao: Joi.string(),
    valor: Joi.number(),
    tempo_gasto: Joi.number(),
    ativo: Joi.boolean()
});