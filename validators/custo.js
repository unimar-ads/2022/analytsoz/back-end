const Joi = require("joi");

exports.allowedUpdate = Joi.object({
    data_lancamento: Joi.date(),
    descricao: Joi.string(),
    valor: Joi.number(),
    fornecedor: Joi.string(),
    ativo: Joi.bool()
});