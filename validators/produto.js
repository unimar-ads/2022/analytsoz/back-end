const Joi = require("joi");

exports.columns = Joi.object({
    nome: Joi.string().required(),
    valor: Joi.number().required(),
});

exports.allowedUpdate = Joi.object({
    nome: Joi.string(),
    valor: Joi.number(),
    ativo: Joi.boolean()
});