const Joi = require("joi");

exports.columns = Joi.object({
    nome: Joi.string().required(),
    cpfcnpj: Joi.string().required(),
    email: Joi.string().required(),
    cidade: Joi.string().required(),
    bairro: Joi.string().required(),
    logradouro: Joi.string().required(),
    cep: Joi.string().required()
});

exports.allowedUpdate = Joi.object({
    nome: Joi.string(),
    cpfcnpj: Joi.string(),
    email: Joi.string(),
    cidade: Joi.string(),
    bairro: Joi.string(),
    logradouro: Joi.string(),
    cep: Joi.string(),
    ativo: Joi.boolean()
});