const db = require("../database");

module.exports = class Venda {
    static insert(usuario_criou, cliente_venda) {
        const query = `
        INSERT INTO analytcoz.vendas(
            usuario_criou,
            cliente_venda
        ) 
        SELECT $1, id FROM analytcoz.clientes
        WHERE id = $2 AND usuario_criou = $1
        RETURNING id
        `;

        return db.query(query, [usuario_criou, cliente_venda]);
    }

    static historicoVendas(usuario_criou) {
        const query = `
        select 
            v.id,
            c.nome,
            v.data_criacao,
            sum(case when iv.id_produto is not null then
                    iv.valor * iv.quantidade
                else 
                    0
                end
            ) + 
            sum(
                case when iv.id_servico is not null then
                    iv.valor * iv.quantidade
                else
                    0
                end
            ) valor
        from analytcoz.vendas v
        inner join analytcoz.itemvenda iv on iv.id_venda = v.id
        inner join analytcoz.clientes  c  on c.id        = v.cliente_venda
        left join analytcoz.produtos   p  on p.id        = iv.id_produto
        left join analytcoz.servicos   s  on s.id        = iv.id_servico
        where v.usuario_criou = $1
        group by 
            v.id,
            c.nome,
            v.data_criacao
        order by v.data_criacao
        `;

        return db.query(query, [usuario_criou]);
    }

    static detalhesVenda(id, usuario_criou) {
        const query = `
        SELECT 
            c.nome,
            v.data_criacao
        FROM analytcoz.vendas v
        INNER JOIN analytcoz.clientes c ON c.id = v.cliente_venda
        WHERE v.id = $1 AND v.usuario_criou = $2
        `;

        return db.query(query, [id, usuario_criou]);
    }

    static produtosVenda(id, usuario_criou) {
        const query = `
        SELECT 
            p.id,
            iv.valor,
            p.nome,
            iv.quantidade
        FROM analytcoz.vendas v
        INNER JOIN analytcoz.itemvenda iv ON iv.id_venda = v.id
        INNER JOIN analytcoz.produtos  p  ON p.id        = iv.id_produto
        WHERE v.id = $1 AND v.usuario_criou = $2
        `;

        return db.query(query, [id, usuario_criou]);
    }

    static servicosVenda(id, usuario_criou) {
        const query = `
        SELECT
            s.id,
            iv.valor, 
            s.descricao,
            iv.quantidade
        FROM analytcoz.vendas v
        INNER JOIN analytcoz.itemvenda iv ON iv.id_venda = v.id
        INNER JOIN analytcoz.servicos  s  ON s.id        = iv.id_servico
        WHERE v.id = $1 AND v.usuario_criou = $2
        `;

        return db.query(query, [id, usuario_criou]);
    }
}