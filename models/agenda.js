const db = require("../database");

module.exports = class Agenda {
    static getAll(usuario_criou) {
        const query = `SELECT * FROM analytcoz.agenda WHERE usuario_criou = $1 AND ativo IS true`;

        return db.query(query, [usuario_criou]);
    }

    static get(usuario_criou, id) {
        const query = `SELECT * FROM analytcoz.agenda WHERE usuario_criou = $1 AND id = $2 AND ativo IS true`;

        return db.query(query, [usuario_criou, id]);
    }

    static insert(usuario_criou, data_inicio, data_fim, titulo, informacoes) {
        const query = `
        INSERT INTO analytcoz.agenda(
            usuario_criou,
            data_inicio,
            data_fim,
            titulo,
            informacoes
        ) VALUES(
            $1,
            $2,
            $3,
            $4,
            $5
        )
        `

        return db.query(query, [usuario_criou, data_inicio, data_fim, titulo, informacoes]);
    }

    static update(id, dados, usuario_criou) {
        let query = `
        UPDATE 
            analytcoz.agenda
        SET
        `;

        const keys = Object.keys(dados);
        const values = Object.values(dados);

        for (let i = 0; i < keys.length; i++) {
            query = query+`${keys[i]} = $${i + 1} ${i + 1 == keys.length ? '' : ','}\n`
        }

        query = query+`WHERE usuario_criou = $${keys.length + 1} AND id = $${keys.length + 2}`;

        values.push(usuario_criou, id);

        return db.query(query, values);
    }
}