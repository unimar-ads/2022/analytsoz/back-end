const db = require("../database");

module.exports = class Custo {
    static getAll(usuario_criou) {
        const query = `SELECT * FROM analytcoz.custos WHERE usuario_criou = $1 AND ativo IS true`;

        return db.query(query, [usuario_criou]);
    }

    static get(usuario_criou, id) {
        const query = `SELECT * FROM analytcoz.custos WHERE usuario_criou = $1 AND id = $2 AND ativo IS true`;

        return db.query(query, [usuario_criou, id]);
    }

    static insert(data_lancamento, descricao, valor, fornecedor, usuario_criou) {
        const query = `
        INSERT INTO analytcoz.custos(
            data_lancamento,
            descricao,
            valor,
            fornecedor,
            usuario_criou
        ) VALUES(
            $1,
            $2,
            $3,
            $4,
            $5
        )
        `
        
        return db.query(query, [data_lancamento, descricao, valor, fornecedor, usuario_criou]);
    }

    static update(id, dados, usuario_criou) {
        let query = `
        UPDATE 
            analytcoz.custos
        SET
        `;

        const keys = Object.keys(dados);
        const values = Object.values(dados);

        for (let i = 0; i < keys.length; i++) {
            query = query+`${keys[i]} = $${i + 1} ${i + 1 == keys.length ? '' : ','}\n`
        }

        query = query+`WHERE usuario_criou = $${keys.length + 1} AND id = $${keys.length + 2}`;

        values.push(usuario_criou, id);

        return db.query(query, values);
    }
}