const db = require("../database");

module.exports = class Faturamento {
    static vendasProdutoServico(usuario_criou, dias) {
        const query = `
        SELECT 
            resultado.categoria,
            resultado.valor_total,
            resultado.total_vendas
        FROM
        (
            SELECT 
                'Produtos' categoria, 
                count(v.id) total_vendas,
                sum(iv.valor * iv.quantidade) valor_total
            FROM
            analytcoz.vendas v
            INNER JOIN analytcoz.itemvenda iv ON iv.id_venda = v.id
            INNER JOIN analytcoz.produtos p ON p.id = iv.id_produto
            WHERE v.usuario_criou = $1 AND EXTRACT(DAY FROM (NOW() - v.data_criacao)) <= $2
            
            UNION
            
            SELECT 
                'Serviços' categoria,
                COUNT(v.id) total_vendas,
                SUM(iv.valor * iv.quantidade) valor_total
            FROM
            analytcoz.vendas v
            INNER JOIN analytcoz.itemvenda iv ON iv.id_venda = v.id
            INNER JOIN analytcoz.servicos s ON s.id = iv.id_servico
            WHERE v.usuario_criou = $1 AND EXTRACT(DAY FROM (NOW() - v.data_criacao)) <= $2
        ) resultado
        `;

        return db.query(query, [usuario_criou, dias]);
    }

    static produtosMaisvendidos(usuario_criou, dias) {
        const query = `
        SELECT 
            p.nome,
            SUM(iv.quantidade) quantidade
        FROM analytcoz.vendas v
        INNER JOIN analytcoz.itemvenda iv ON iv.id_venda = v.id
        INNER JOIN analytcoz.produtos p ON p.id = iv.id_produto
        WHERE v.usuario_criou = $1 AND EXTRACT(DAY FROM (NOW() - v.data_criacao)) <= $2
        GROUP BY p.nome
        ORDER BY quantidade DESC
        LIMIT 5
        `;

        return db.query(query, [usuario_criou, dias]);
    }

    static servicosMaisvendidos(usuario_criou, dias) {
        const query = `
        SELECT 
            s.descricao,
            SUM(iv.quantidade) quantidade
        FROM analytcoz.vendas v
        INNER JOIN analytcoz.itemvenda iv ON iv.id_venda = v.id
        INNER JOIN analytcoz.servicos s ON s.id = iv.id_servico
        WHERE v.usuario_criou = $1 AND EXTRACT(DAY FROM (NOW() - v.data_criacao)) <= $2
        GROUP BY s.descricao
        ORDER BY quantidade DESC
        LIMIT 5
        `;

        return db.query(query, [usuario_criou, dias]);
    }
}