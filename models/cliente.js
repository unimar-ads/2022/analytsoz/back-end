const db = require("../database");

module.exports = class Cliente {
    static getAll(usuario_criou) {
        const query = `SELECT * FROM analytcoz.clientes WHERE usuario_criou = $1 AND ativo IS true`;
        
        return db.query(query, [usuario_criou]);
    }

    static get(usuario_criou, id) {
        const query = `SELECT * FROM analytcoz.produtos WHERE usuario_criou = $1 AND id = $2 AND ativo is true`;

        return db.query(query, [usuario_criou, id]);
    }

    static insert(nome, cpfcnpj, email, cidade, bairro, logradouro, cep, usuario_criou) {
        nome = decodeURI(nome);
        cidade = decodeURI(cidade);
        bairro = decodeURI(bairro);
        logradouro = decodeURI(logradouro);

        const query = `
        INSERT INTO analytcoz.clientes(
            nome,
            cpfcnpj,
            email,
            cidade,
            bairro,
            logradouro,
            cep,
            usuario_criou
        ) VALUES(
            $1,
            $2,
            $3,
            $4,
            $5,
            $6,
            $7,
            $8
        )
        `

        return db.query(query, [nome, cpfcnpj, email, cidade, bairro, logradouro, cep, usuario_criou]);
    }

    static update(id, dados, usuario_criou) {
        let query = `
        UPDATE 
            analytcoz.clientes
        SET
        `;

        const keys = Object.keys(dados);
        const values = Object.values(dados);

        for (let i = 0; i < keys.length; i++) {
            query = query+`${keys[i]} = $${i + 1} ${i + 1 == keys.length ? '' : ','}\n`
        }

        query = query+`WHERE usuario_criou = $${keys.length + 1} AND id = $${keys.length + 2}`;

        values.push(usuario_criou, id);

        return db.query(query, values);
    }
}