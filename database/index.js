require("dotenv").config();
const { Pool } = require("pg");

const types = require('pg').types
types.setTypeParser(20, function(val) {
  return parseInt(val, 10)
})

module.exports = new Pool({
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_DATABASE,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    ssl: {
        rejectUnauthorized: false
    }
});